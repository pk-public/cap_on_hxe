using us.np as np from '../db/data-model';

service USNPService {
    entity States @readonly as projection on np.States;
    entity AreaTypes @readonly  as projection on np.AreaTypes;
    entity Areas @readonly as projection on np.Areas;
    entity AreaAnnualVisitations @readonly as projection on np.AreaAnnualVisitations;
}