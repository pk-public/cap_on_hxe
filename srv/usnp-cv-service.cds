using AnnualVisits as VisitsPerYear from '../db/import';
using AnnualVisitsPerArea as VisitsPerAreaAndYear from '../db/import';
using AnnualVisitsPerState as VisitsPerStateAndYear from '../db/import';
using AnnualVisitsPerAreaType as VisitsPerAreaTypeAndYear from '../db/import';
using AnnualVisitsPerAreaTypeAndState as VisitsPerAreaTypeAndStateAndYear from '../db/import';

service USNPCVService {
   entity AnnualVisits @readonly as projection on VisitsPerYear; 
   entity AnnualVisitsPerArea @readonly as projection on VisitsPerAreaAndYear;
   entity AnnualVisitsPerState @readonly as projection on VisitsPerStateAndYear; 
   entity AnnualVisitsPerAreaType @readonly as projection on VisitsPerAreaTypeAndYear;
   entity AnnualVisitsPerAreaTypeAndState @readonly as projection on VisitsPerAreaTypeAndStateAndYear;
}
