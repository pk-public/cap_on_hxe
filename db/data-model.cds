namespace us.np;

type StateCode : String(2);
type StateCodeISO: String(5);
type AreaTypeCode: String(5);
type AreaCode: String(4);
type StateStatus: String(30);
type StateName: String(30);
type Abbreviation: String(10);
type CityName: String(30);
type Description: String(50);
type AreaName: String(50);
type CalendarYear: String(4);

entity States {
    key stateCode: StateCode;
    stateCodeISO: StateCodeISO;
    status: StateStatus;
    stateName: StateName;
    abbreviation: Abbreviation;
    capital: CityName;
    largestCity: CityName;
    population: Integer;
    areaMI2: Integer;
    areaKM2: Integer;
    landAreaMI2: Integer;
    landAreaKM2: Integer;
    numberOfReps: Integer;
    areas  : Association to many Areas on areas.state = $self;
}

entity AreaTypes {
    key areaTypeCode: AreaTypeCode;
    description: Description;
    areas  : Association to many Areas on areas.areaType = $self;
}

entity Areas {
    key areaCode: AreaCode;
    areaName: AreaName;
    description: Description;
    stateCode: StateCode;
    areaTypeCode: AreaTypeCode;
    grossAreaAcres: Decimal(15,2);
    grossAreaKM2: Decimal(15,10);
    state: Association to States on state.stateCode = $self.stateCode;
    areaType: Association to AreaTypes on areaType.areaTypeCode = $self.areaTypeCode; 
}

entity AreaAnnualVisitations {
    key areaCode: AreaCode;
    key calyear: CalendarYear;
    visits: Integer;
    visitsChange: Integer;
    area: Association to Areas on area.areaCode = $self.areaCode;
}
