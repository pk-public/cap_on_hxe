@cds.persistence.exists
Entity AnnualVisits {
	key CALYEAR: String(4);
	VISITS: Integer;
	VISITSCHANGE: Integer;
}

@cds.persistence.exists
Entity AnnualVisitsPerArea {
	key AREACODE: String(5);
	AREANAME: String(50);
	key CALYEAR: String(4);
	GROSSAREAACRES: Decimal(15,2);
	STATECODE: String(2);
	AREATYPECODE: String(5);
	VISITSPERAREATYPE: Integer;
	VISITSPERSTATE: Integer;
	VISITSPERAREATYPEANDSTATE: Integer;
	VISITS: Integer;
	VISITSCHANGE: Integer;
}

@cds.persistence.exists
Entity AnnualVisitsPerState {
	key STATECODE: String(2);
	STATENAME: String(30);
	key CALYEAR: String(4);
	VISITS: Integer;
	VISITSCHANGE: Integer;
}

@cds.persistence.exists
Entity AnnualVisitsPerAreaType {
	key AREATYPECODE: String(5);
	DESCRIPTION: String(50);
	key CALYEAR: String(4);
	VISITS: Integer;
	VISITSCHANGE: Integer;
}

@cds.persistence.exists
Entity AnnualVisitsPerAreaTypeAndState {
	key AREATYPECODE: String(5);
	key STATECODE: String(2);
	DESCRIPTION: String(50);
	STATENAME: String(30);
	key CALYEAR: String(4);
	VISITS: Integer;
	VISITSCHANGE: Integer;
	VISITSPERAREATYPE: Integer;
	VISITSPERSTATE: Integer;
}