sap.ui.define([
	"us/np/Dashboard/controller/BaseController",
	"sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("us.np.Dashboard.controller.Dashboard", {
		onInit: function () {
			this.sSelectedYear = "";
			this.oSelectYear = this.getById("slYear");
			this.sSelectedAreaType = "";
			this.sSelectedAreaTypeText = "All area types";
			this.oSelectAreaType = this.getById("slAreaType");
			this.sSelectedState = "";
			this.sSelectedStateText = "All states";
			this.oSelectState = this.getById("slState");
			this.oCardAnnualVisitsTimeSeries = this.getById("cardAnnualVisitsTimeSeries");
			this.oCardTop5AreaTypes = this.getById("cardTop5AreaTypes");
			this.oCardTop5Areas = this.getById("cardTop5Areas");

			this.getFilterBarData();
			
			this.oCardsModel = new JSONModel();
			this.oCardsModel.loadData(sap.ui.require.toUrl("us/np/Dashboard/model/cardManifests.json"));
			this.setModel(this.oCardsModel, "manifests");
		},
		
		getFilterBarData: function () {
			var _this = this;
			var oYearsModel = new JSONModel();
			var oAreaTypesModel = new JSONModel();
			var oStatesModel = new JSONModel();
			var oStatesAreaTypesModel = new JSONModel();
			var oModel = new JSONModel();
			oModel.setSizeLimit(2000);
			oModel.attachRequestSent(function () {
				_this.getView().setBusy(true);
			});
			oModel.attachRequestCompleted(function () {
				_this.getView().setBusy(false);
				var aYears = [];
				var oData = {};
				oData.years = [];
				oData.areaTypes = [];
				oData.states = [];
				oData.statesAreaTypes = [];
				var aData = oModel.getData().value;
				var oAllAreaTypes = {};
				oAllAreaTypes.key = "";
				oAllAreaTypes.text = "All area types";
				oData.areaTypes.push(oAllAreaTypes);
				var oAllStates = {};
				oAllStates.key = "";
				oAllStates.text = "All states";
				oData.states.push(oAllStates);
				aData.forEach(function (element) {
					if (element.AREATYPECODE === "IHS" && element.STATECODE === "ME") {
						var oYear = {};
						oYear.key = element.CALYEAR;
						oYear.text = element.CALYEAR;
						aYears.push(oYear);
					}
					if (oData.areaTypes.find(function (a) { return a.key === element.AREATYPECODE; }) === undefined) {
						var oAreaType = {};
						oAreaType.key = element.AREATYPECODE;
						oAreaType.text = element.DESCRIPTION;
						oData.areaTypes.push(oAreaType);
					}
					if (oData.states.find(function (s) { return s.key === element.STATECODE; }) === undefined) {
						var oState = {};
						oState.key = element.STATECODE;
						oState.text = element.STATENAME;
						oData.states.push(oState);
					}
					if (oData.statesAreaTypes.find(function (e) { return (e.stateCode === element.STATECODE && 
						                                         e.areaTypeCode === element.AREATYPECODE); }) === undefined) {
						var oStateAreaType = {};
						oStateAreaType.stateCode = element.STATECODE;
						oStateAreaType.areaTypeCode = element.AREATYPECODE;
						oStateAreaType.stateName = element.STATENAME;
						oStateAreaType.description = element.DESCRIPTION;
						oData.statesAreaTypes.push(oStateAreaType);
					}
				});
				oData.years = aYears.sort(function(a, b){
					return b.key - a.key;
				});
				if (oData.years.length > 0) {
					_this.sSelectedYear = oData.years[0].key;
				}
				oYearsModel.setData(oData.years);
				_this.setModel(oYearsModel, "years");
				oData.states = oData.states.sort(function(a,b){
					if (a.key === b.key) { return 0; }
					if (a.key > b.key) { return 1; }
					return -1;
				});
				oAreaTypesModel.setData(oData.areaTypes);
				_this.setModel(oAreaTypesModel, "areaTypes");
				oStatesModel.setData(oData.states);
				_this.setModel(oStatesModel, "states");
				oStatesAreaTypesModel.setData(oData.statesAreaTypes);
				_this.setModel(oStatesAreaTypesModel, "statesAreaTypes");
				var oParameters = {
					"urlModifier": "?$filter=CALYEAR le '" + _this.sSelectedYear + "'&$orderby=CALYEAR",
					"urlModifierHeader": "('" + _this.sSelectedYear + "')",
					"year": _this.sSelectedYear,
					"areaType": "",
					"areaTypeDescription": "All area types",
					"state": "",
					"stateName": "All states"
				};
				_this.oCardAnnualVisitsTimeSeries.setParameters(oParameters);
				var oParametersAreaTypes = {
					"urlModifier": "?$filter=CALYEAR eq '" + _this.sSelectedYear + "'&$orderby=VISITS desc&$top=5",
					"year": _this.sSelectedYear,
					"state": "",
					"stateName": "All states"
				};
				_this.oCardTop5AreaTypes.setParameters(oParametersAreaTypes);
				var oParametersAreas = {
					"urlModifier": "?$filter=CALYEAR eq '" + _this.sSelectedYear + "'&$orderby=VISITS desc&$top=5",
					"year": _this.sSelectedYear,
					"state": "",
					"stateName": "All states"
				};
				_this.oCardTop5Areas.setParameters(oParametersAreas);
            });
            oModel.loadData("/usnpcv/AnnualVisitsPerAreaTypeAndState?$orderby=CALYEAR desc");
		}, 
		
		onYearChange: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");			
			this.sSelectedYear = oSelectedItem.getProperty("key");
			this.setCardParameters();
		},
		
		onAreaTypeChange: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");			
			this.sSelectedAreaType = oSelectedItem.getProperty("key");
			this.sSelectedAreaTypeText = oSelectedItem.getProperty("text");
			this.setCardParameters();
			this.setSelectValues();
		},

		onStateChange: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");			
			this.sSelectedState = oSelectedItem.getProperty("key");
			this.sSelectedStateText = oSelectedItem.getProperty("text");
			this.setCardParameters();
			this.setSelectValues();
		},
		
		setCardParameters: function () {
			var urlModifier = "";
			var urlModifierHeader = "";
			var urlModifierAreaTypes = "";
			var urlModifierAreas = "";
			if (this.sSelectedAreaType.length === 0) {
				if (this.sSelectedState.length === 0) {
					urlModifier = "?$filter=CALYEAR le '" + this.sSelectedYear + "'&$orderby=CALYEAR";
					urlModifierHeader = "('" + this.sSelectedYear + "')";
					urlModifierAreaTypes = "?$filter=CALYEAR eq '" + this.sSelectedYear + "'&$orderby=VISITS desc&$top=5";
					urlModifierAreas = "?$filter=CALYEAR eq '" + this.sSelectedYear + "'&$orderby=VISITS desc&$top=5";
				} else {
					urlModifier = "PerState?$filter=STATECODE eq '" + this.sSelectedState + "'" 
					              + " and CALYEAR le '" + this.sSelectedYear + "'&$orderby=CALYEAR";
					urlModifierHeader = "PerState(CALYEAR='" + this.sSelectedYear + "',STATECODE='" + this.sSelectedState + "')";
					urlModifierAreaTypes = "AndState?$filter=CALYEAR eq '" + this.sSelectedYear + "'"
										   + " and STATECODE eq '" + this.sSelectedState + "'&$orderby=VISITS desc&$top=5";
					urlModifierAreas = "?$filter=CALYEAR eq '" + this.sSelectedYear + "'"
									   + " and STATECODE eq '" + this.sSelectedState + "'"
									   + "&$orderby=VISITS desc&$top=5";
				}
			} else {
				if (this.sSelectedState.length === 0) {
					urlModifier = "PerAreaType?$filter=AREATYPECODE eq '" + this.sSelectedAreaType + "'"
					              + " and CALYEAR le '" + this.sSelectedYear + "'&$orderby=CALYEAR";
					urlModifierHeader = "PerAreaType(CALYEAR='" + this.sSelectedYear + "',AREATYPECODE='" + this.sSelectedAreaType + "')";
					urlModifierAreaTypes = "?$filter=CALYEAR eq '" + this.sSelectedYear + "'" 
					                       + " and AREATYPECODE eq '" + this.sSelectedAreaType + "'" 
					                       + "&$orderby=VISITS desc&$top=5";
					urlModifierAreas = "?$filter=CALYEAR eq '" + this.sSelectedYear + "'" 
									   + " and AREATYPECODE eq '" + this.sSelectedAreaType + "'" 
									   + "&$orderby=VISITS desc&$top=5";
				} else {
					urlModifier = "PerAreaTypeAndState?$filter=AREATYPECODE eq '" + this.sSelectedAreaType + "' and " 
								  + "STATECODE eq '" + this.sSelectedState + "'"
								  + " and CALYEAR le '" + this.sSelectedYear + "'&$orderby=CALYEAR";
					urlModifierHeader = "PerAreaTypeAndState(CALYEAR='" + this.sSelectedYear + "',AREATYPECODE='" + this.sSelectedAreaType 
										+ "',STATECODE='" + this.sSelectedState + "')";
					urlModifierAreaTypes = "AndState?$filter=CALYEAR eq '" + this.sSelectedYear + "'"
											+ " and AREATYPECODE eq '" + this.sSelectedAreaType + "'"
					     					+ " and STATECODE eq '" + this.sSelectedState + "'&$orderby=VISITS desc&$top=5";
					urlModifierAreas = "?$filter=CALYEAR eq '" + this.sSelectedYear + "'"
					                    + " and AREATYPECODE eq '" + this.sSelectedAreaType + "'"
										+ " and STATECODE eq '" + this.sSelectedState + "'"
										+ "&$orderby=VISITS desc&$top=5";								
				}
			}
			var oParameters = {
				"urlModifier": urlModifier,
				"urlModifierHeader": urlModifierHeader,
				"year": this.sSelectedYear,
				"areaType": this.sSelectedAreaType,
				"areaTypeDescription": this.sSelectedAreaTypeText,
				"state": this.sSelectedState,
				"stateName": this.sSelectedStateText
			};
			this.oCardAnnualVisitsTimeSeries.setParameters(oParameters);
			var oParametersAreaTypes = {
				"urlModifier": urlModifierAreaTypes,
				"year": this.sSelectedYear,
				"state": this.sSelectedState,
				"stateName": this.sSelectedStateText
			};
			this.oCardTop5AreaTypes.setParameters(oParametersAreaTypes);
			var oParametersAreas = {
				"urlModifier": urlModifierAreas,
				"year": this.sSelectedYear,
				"areaType": this.sSelectedAreaType,
				"areaTypeDescription": this.sSelectedAreaTypeText,
				"state": this.sSelectedState,
				"stateName": this.sSelectedStateText
			};
			this.oCardTop5Areas.setParameters(oParametersAreas);
		},
		
		setSelectValues: function () {
			var _this = this;
			var aStates = [];
			var aAreaTypes = [];
			var oAllAreaTypes = {};
			oAllAreaTypes.key = "";
			oAllAreaTypes.text = "All area types";
			aAreaTypes.push(oAllAreaTypes);				
			var oAllStates = {};
			oAllStates.key = "";
			oAllStates.text = "All states";
			aStates.push(oAllStates);
			var aStatesAreaTypes = this.getModel("statesAreaTypes").getData();
			aStatesAreaTypes.forEach(function (element) {
				if (_this.sSelectedState.length === 0 || element.stateCode === _this.sSelectedState) {
					if (aAreaTypes.find(function (a) { return a.key === element.areaTypeCode; }) === undefined) {
						var oAreaType = {};
						oAreaType.key = element.areaTypeCode;
						oAreaType.text = element.description;
						aAreaTypes.push(oAreaType);
					}		
				}
				if (_this.sSelectedAreaType.length === 0 || element.areaTypeCode === _this.sSelectedAreaType) {
					if (aStates.find(function (s) { return s.key === element.stateCode; } ) === undefined) {
						var oState = {};
						oState.key = element.stateCode;
						oState.text = element.stateName;
						aStates.push(oState);
					}
				}
			});

			aStates = aStates.sort(function(a,b){
				if (a.key === b.key) { return 0; }
				if (a.key > b.key) { return 1; }
				return -1;
			});
			aAreaTypes = aAreaTypes.sort(function(a,b){
				if (a.key === b.key) { return 0; }
				if (a.key > b.key) { return 1; }
				return -1;
			});

			var oAreaTypesModel = this.getModel("areaTypes");
			var oStatesModel = this.getModel("states");
			oAreaTypesModel.setData(aAreaTypes);
			oStatesModel.setData(aStates);
		},
		
		getById: function (sId) {
			return this.getView().byId(sId);
		},

		onExit: function () {
			this.oYearsModel = null;
			this.oCardsModel = null;
		}	
	});
});